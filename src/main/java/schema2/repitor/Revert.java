package schema2.repitor;

import schema2.elements.AbstractElement;
import schema2.elements.Power;

public class Revert extends AbstractRepeater {
    public Revert(Power power) {
        super(power);
    }

    public void calculateResultState() {
        resultState = power.powerIsOn() && !switchState.powerIsOn() ?
                Power.ON : Power.OFF;
    }
}
