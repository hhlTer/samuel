package schema2.repitor;

import schema2.elements.Power;

public interface RepeaterInterface extends BaseElement {
    void switchTo(Power switchTo);
}
