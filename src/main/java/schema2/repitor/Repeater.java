package schema2.repitor;

import schema2.elements.Power;

import static schema2.elements.Power.OFF;
import static schema2.elements.Power.ON;

public class Repeater extends AbstractRepeater {

    public Repeater(Power power) {
        super(power);
    }

    public Repeater() {
        this(ON);
    }

    public void calculateResultState() {
        resultState = power.powerIsOn() && switchState.powerIsOn() ?
                ON : OFF;
    }
}
