package schema2.repitor;

import schema2.elements.Power;

import static schema2.elements.Power.OFF;

public abstract class AbstractRepeater implements RepeaterInterface {

    AbstractRepeater(Power power){
        this.power = power;
        this.switchState = OFF;
        this.resultState = OFF;
    }

    protected Power power;
    protected Power switchState;
    protected Power resultState;

    public void powerOn(Power power) {
        this.power = power;
        calculateResultState();
    }
    public void switchTo(Power switchTo) {
        this.switchState = switchTo;
        calculateResultState();
    }

    public Power getResultState() {
        return resultState;
    }
}
