package schema2.elements;

import com.google.gson.reflect.TypeToken;
import schema2.repitor.AbstractRepeater;
import schema2.repitor.Repeater;
import schema2.repitor.RepeaterInterface;

import java.lang.reflect.InvocationTargetException;

public abstract class AbstractElement<T extends AbstractRepeater> implements LogicalInterface {

    protected Power resultState;
    protected RepeaterInterface repeaterA;
    protected RepeaterInterface repeaterB;

    public AbstractElement(Class<T> clazz, Power power){
        try {
            repeaterA = clazz.getConstructor(Power.class).newInstance(power);
            repeaterB = clazz.getConstructor(Power.class).newInstance(power);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public AbstractElement(RepeaterInterface repeaterA, RepeaterInterface repeaterB){
        this.repeaterA = repeaterA;
        this.repeaterB = repeaterB;
        resultState = Power.OFF;
    }


    public void powerOn(Power power){
        repeaterA.powerOn(power);
        repeaterB.powerOn(repeaterA.getResultState());
        calculateResultState();
    }

    public void switchA(Power switchTo){
        repeaterA.switchTo(switchTo);
        repeaterB.powerOn(repeaterA.getResultState());
        calculateResultState();
    }

    public void switchB(Power switchTo){
        repeaterB.switchTo(switchTo);
        calculateResultState();
    }

    public Power getResultState(){
        return resultState;
    }


}
