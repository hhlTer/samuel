package schema2.elements;

public enum Power {
    ON("true"), OFF("false");

    private boolean values;

    Power(String values) {
        this.values = Boolean.valueOf(values);
    }

    public boolean powerIsOn() {
        return values;
    }


}
