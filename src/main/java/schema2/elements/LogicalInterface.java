package schema2.elements;

import schema2.repitor.BaseElement;

public interface LogicalInterface extends BaseElement {
    void switchA(Power switchTo);
    void switchB(Power switchTo);
}
