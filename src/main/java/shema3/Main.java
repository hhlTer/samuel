package shema3;

import schema2.elements.LogicalInterface;
import schema2.elements.Power;
import schema2.repitor.Revert;
import shema3.element.AbstractElement;
import shema3.element.And;
import shema3.element.Or;
import shema3.repeator.Repeater;
import shema3.repeator.Reverter;

import static schema2.elements.Power.OFF;
import static schema2.elements.Power.ON;

public class Main {
    public static void main(String[] args) {
        And and = new And(Repeater.class);
        and.powerOn(Power.ON);
        System.out.println(and + " exp: OFF");
        andTest(and);
        and.switchB(ON);
        System.out.println(and);
        and.switchA(ON);
        System.out.println(and + "exp: ON");
    }

    private static void andTest(AbstractElement and) {
        for (int i = 3; i > 0; i--) {
            String s1 = Integer.toBinaryString(i);
            String s = s1.length() < 2 ? "0" + s1
                    : s1;

            and.switchA(getStateByString(s.split("")[0]));
            and.switchB(getStateByString(s.split("")[1]));

            System.out.println(s);
            System.out.println(and);
        }
    }


    private static Power getStateByString(String s){
        int i = Integer.parseInt(s);
        return i == 1 ? ON : i == 0 ? OFF : null;
    }
}
