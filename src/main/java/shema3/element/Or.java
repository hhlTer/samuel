package shema3.element;

import schema2.elements.Power;
import shema3.repeator.Repeater;

public class Or extends AbstractElement<Repeater> {
    public Or(Class<Repeater> repeater) {
        super(repeater);
    }

    protected void switchAEvent() {
        calculateResult();
    }

    protected void switchBEvent() {
        calculateResult();
    }

    protected void calculateResult() {
        this.resultState = this.repeaterA.getResultState() != this.repeaterB.getResultState() ? Power.ON
                : repeaterA.getResultState();
    }

    @Override
    public String toString() {
        return "Or{" +
                "repeaterA=" + repeaterA +
                ", repeaterB=" + repeaterB +
                ", resultState=" + resultState +
                '}';
    }
}
