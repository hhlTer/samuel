package shema3.element;

import schema2.elements.Power;
import shema3.repeator.AbstractRepeater;
import shema3.repeator.InterfaceRepeator;

import java.lang.reflect.InvocationTargetException;

public abstract class AbstractElement<T extends AbstractRepeater> {

    protected AbstractRepeater repeaterA;
    protected AbstractRepeater repeaterB;
    protected Power resultState;

    public AbstractElement(Class<T> repeater){
        try {
            this.repeaterA = repeater.getConstructor().newInstance();
            this.repeaterB = repeater.getConstructor().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        powerOn(Power.OFF);
    }

    public void powerOn(Power on){
        this.repeaterA.powerOn(on);
        switchAEvent();
    }

    public void switchA(Power on){
        this.repeaterA.switchOn(on);
        switchAEvent();
    }

    public void switchB(Power on){
        this.repeaterB.switchOn(on);
        switchBEvent();
    }

    public Power getResultState(){
        return resultState;
    }

    protected abstract void switchAEvent();
    protected abstract void switchBEvent();
//    protected abstract void calculateResult();
}
