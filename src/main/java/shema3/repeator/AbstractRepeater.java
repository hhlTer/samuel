package shema3.repeator;

import schema2.elements.Power;

public abstract class AbstractRepeater implements InterfaceRepeator {

    protected Power resultState;
    protected Power powerState;
    protected Power switchState;

    public AbstractRepeater(){
        this.powerState = Power.OFF;
        this.resultState = Power.OFF;
        this.switchState = Power.OFF;
        calculateResult();
    }

    public void powerOn(Power on) {
        powerState = on;
        calculateResult();
    }

    public void switchOn(Power on) {
        switchState = on;
        calculateResult();
    }

    public Power getResultState() {
        return resultState;
    }
}
