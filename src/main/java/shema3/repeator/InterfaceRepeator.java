package shema3.repeator;

import schema2.elements.Power;

public interface InterfaceRepeator {
    void powerOn(Power on);
    void switchOn(Power on);
    void calculateResult();
}
