package shema3.repeator;

import static schema2.elements.Power.OFF;
import static schema2.elements.Power.ON;

public class Reverter extends AbstractRepeater {

    public Reverter(){
        super();
    }

    public void calculateResult() {
        this.resultState = this.powerState.powerIsOn() & !this.switchState.powerIsOn() ?
                ON : OFF;
    }

    @Override
    public String toString() {
        return "\nReverter{" +
                "\nresultState=" + resultState +
                ", \npowerState=" + powerState +
                ", \nswitchState=" + switchState +
                '}';
    }
}
