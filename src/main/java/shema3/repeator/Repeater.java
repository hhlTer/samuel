package shema3.repeator;

import schema2.elements.Power;

import static schema2.elements.Power.OFF;
import static schema2.elements.Power.ON;

public class Repeater extends AbstractRepeater {

    public Repeater(){
        super();
    }

    public void calculateResult() {
        this.resultState = this.powerState.powerIsOn() ? this.switchState : OFF;
    }

    @Override
    public String toString() {
        return "\nRepeater{" +
                "\nresultState=" + resultState +
                ",\n powerState=" + powerState +
                ",\n switchState=" + switchState +
                '}';
    }
}
